# SparseTransform

This is a program to transform sparse matrices from the SuiteSparse matrix collection to DOT graphs and perform coloring on them.

## How to Run

Using Microsoft Visual Studio 2022: Open `SparseTransform.sln`, set the `SparseTransform.CLI` project as default and run using the provided interface tools. This might be done by pressing <kbd>CTRL</kbd> + <kbd>F5</kbd>.

> The instructions below assume that your working directory is at the project root

Using the .NET SDK command `dotnet`: Execute

`dotnet run --project ./src/SparseTransform.CLI/SparseTransform.CLI.csproj -- <ARGS>`

\<ARGS> are the commands and arguments to pass depending on the task you want to perform. For example to read a MatrixMarket string from a file, partition it and output the result as a colored DOT graph, use the following arguments:

`partition -f dot -i inFile -o outFile`.

> Make sure you specify the `SparseTransform.CLI` project as the library project is not runnable by itself.

## How to Build the Program Into an Executable

> The instructions below assume that your working directory is at the project root

Using the .NET SDK command `dotnet` you can choose between building for execution with an installed .NET framework or as a standalone executable.

### Using .NET Framework:

```
dotnet publish ./src/SparseTransform.CLI/SparseTransform.CLI.csproj -r <RID> -o SparseTransform --self-contained false
```

### As a Standalone Executable: 

```
dotnet publish ./src/SparseTransform.CLI/SparseTransform.CLI.csproj -r <RID> -o SparseTransform
```

\<RID> specifies the Runtime-ID that you want to build for (e.g. win-x64 or linux-x64). For available options see the [.NET RID Catalog](https://docs.microsoft.com/en-us/dotnet/core/rid-catalog).

Both commands output to the SparseTransform directory at the project root. The output path can be changed as desired.

The project can also be built using the Visual Studio interface. Right click on the SparseTransform.CLI project and select _Publish_. Then set the flags accordingly.

## How to Build the Documentation

We currently use [Doxygen](https://www.doxygen.nl/) to generate an HTML documentation as well as the LaTeX files needed to generate a PDF manual. Make sure you have a working installation of Doxygen installed.

> The instructions below assume that your working directory is in the _/docs_ folder located in the project root.

To build both documentations execute

```
doxygen Doxyfile
```

This will generate 2 folders: `html` and `latex`. The `html` folder contains the HTML source. You can open it by opening the file `index.html` in your browser.

The `latex` folder contains LaTeX source to generate a PDF manual. You will need either [Make](https://www.gnu.org/software/make/) or [`latexmk`](https://www.ctan.org/pkg/latexmk/) to ensure the right number of pdflatex runs.

To compile the files into a single PDF document using Make execute

```
make
```

If you do not have Make installed use `latexmk` (Obtainable e.g. via the MiKTeX installer):

```
latexmk refman
```

The compiled PDF file is named `refman.pdf` and also stored in the `latex` folder.

## How to Run the NUnit Test Suite

> The instructions below assume that your working directory is at the project root

This project uses [NUnit](https://nunit.org/) for unit testing. The complete test suite can be run using the dotnet command

```
dotnet test
```

or the Visual Studio interface.