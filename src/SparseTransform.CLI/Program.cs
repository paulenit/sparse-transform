﻿using CommandLine;
using SparseTransform;
using CLIOptions;

public class Program

{
    public static void Main(String[] args)
    {
        Parser.Default.ParseArguments<ConvertOptions, PartitionOptions>(args)
            .WithParsed<ConvertOptions>(o =>
            {
                try
                {
                    String MMMatrix = File.ReadAllText(o.Input);
                    String DotString = Convert2Dot(MMMatrix);
                    File.WriteAllText(o.Output, DotString);
                }
                catch (Exception e)
                {
                    Console.Out.WriteLine(e.Message);
                    return;
                }

            })
            .WithParsed<PartitionOptions>(o =>
            {
                try
                {
                    String MMMatrix = File.ReadAllText(o.Input);
                    String Output = "";

                    if (o.Format == Formats.ColoredDot)
                    {
                        Output = Partition2ColoredDot(MMMatrix, o.TextColor);
                    }
                    else if (o.Format == Formats.SeedMatrix)
                    {
                        Output = Partition2SeedMatrix(MMMatrix);
                    }
                    else if (o.Format == Formats.CompressedMatrix)
                    {
                        Output = Partition2CompressedMatrix(MMMatrix);
                    }

                    File.WriteAllText(o.Output, Output);
                }
                catch (Exception e)
                {
                    Console.Out.WriteLine(e.Message);
                    return;
                }
            });
    }

    /// <summary>
    /// Reference to the Convert2Dot method from ConversionManager
    /// </summary>
    /// <param name="MMMatrix">raw MatrixMarket input String</param>
    /// <returns>Dot String</returns>    
    private static String Convert2Dot(String MMMatrix)
    {
        return ConversionManager.Convert2Dot(MMMatrix);
    }

    /// <summary>
    /// Reference to the Partition2ColoredDot method from ConversionManager
    /// </summary>
    /// <param name="MMMatrix">raw MatrixMarket input String</param>
    /// <param name="textColor">true if color should additionally be represented as text</param>
    /// <returns>Dot String with coloring information</returns>
    private static String Partition2ColoredDot(String MMMatrix, bool textColor = false)
    {
        return ConversionManager.Partition2ColoredDot(MMMatrix, textColor);
    }

    /// <summary>
    /// Reference to the Partition2SeedMatrix method from ConversionManager
    /// </summary>
    /// <param name="MMMatrix">raw MatrixMarket input String</param>
    /// <returns>String of SeedMatrix</returns>
    private static String Partition2SeedMatrix(String MMMatrix)
    {
        return ConversionManager.Partition2SeedMatrix(MMMatrix);
    }

    /// <summary>
    /// Reference to the Partition2CompressedMatrix method from ConversionManager
    /// </summary>
    /// <param name="MMMatrix">raw MatrixMarket input String</param>
    /// <returns>String of CompressedMatrix</returns>
    private static String Partition2CompressedMatrix(String MMMatrix)
    {
        return ConversionManager.Partition2CompressedMatrix(MMMatrix);
    }
}