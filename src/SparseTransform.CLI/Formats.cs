/// <summary>
/// Permitted values for Format CLI option. Checked in Program.
/// </summary>
public static class Formats
{
    public static readonly String ColoredDot = "dot";
    public static readonly String SeedMatrix = "seed";
    public static readonly String CompressedMatrix = "compressed"; 
}