using CommandLine;

namespace CLIOptions;
/// <summary>
/// CLI options for the `partition` command.
/// </summary>
[Verb("partition", HelpText = "Partitions MatrixMarket String")]
public class PartitionOptions
{
    [Option('i', "input", Required = true, HelpText = "Path to the file containing the MatrixMarket string to read.")]
    public String Input { get; set; }

    [Option('o', "output", Required = true, HelpText = "Path to the file the output should be written to.")]
    public String Output { get; set; }

    [Option('f', "format", Required = true, HelpText = "Format to output as after partitioning. Can be 'dot', 'seed', or 'compressed'.")]
    public String Format { get; set; }

    [Option("textColor", Default = false, HelpText = "Set to true to additionally output the color information as a text label. Default: false")]
    public bool TextColor { get; set; }
}