using CommandLine;

namespace CLIOptions;
/// <summary>
/// CLI options for the `convert` command.
/// </summary>
[Verb("convert", HelpText = "Convert MatrixMarket String to Dot String")]
public class ConvertOptions
{
    [Option('i', "input", Required = true, HelpText = "Path to the file containing the MatrixMarket string to read.")]
    public String Input{ get; set; }

    [Option('o', "output", Required = true, HelpText = "Path to the file the output should be written to.")]
    public String Output{ get; set; }

}