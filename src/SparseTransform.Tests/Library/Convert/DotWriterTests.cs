﻿using System;
using NUnit.Framework;
using SparseTransform.DataStructures;
using SparseTransform.Convert;


namespace SparseTransform.Tests.Library.Convert
{
    public class DotWriterTests
    {
  
        
 
        [Test]
        public void writerTestBipartite()
        {
            DotWriter writer = new DotWriter();

            BipartiteGraph graph = new BipartiteGraph();
            graph.AddEdge(1, 1);
            graph.AddEdge(1, 2);
            graph.AddEdge(1, 5);
            graph.AddEdge(2, 3);
            graph.AddEdge(3, 2);
            graph.AddEdge(3, 3);
            graph.AddEdge(3, 4);
            graph.AddEdge(4, 1);
            graph.AddEdge(5, 4);
            graph.AddEdge(5, 5);

            String[] lines = writer.Write(graph).Split(
            new String[] { "\r\n", "\r", "\n" },
            StringSplitOptions.None
            );

            Assert.That(lines[2], Is.EqualTo("\tr_1  [shape=circle, style=filled]"));
            Assert.That(lines[4], Is.EqualTo("\tr_3  [shape=circle, style=filled]"));
            Assert.That(lines[5], Is.EqualTo("\tr_4  [shape=circle, style=filled]"));
            Assert.That(lines[13], Is.EqualTo("\tc_2 -- {r_1 r_3};"));
            Assert.That(lines[15], Is.EqualTo("\tc_4 -- {r_3 r_5};"));
        }



        [Test]
        public void writerTestAdjacency()
        {
            DotWriter writer = new DotWriter();

            AdjacencyGraph graph = new AdjacencyGraph();
            graph.AddEdge(2, 1);
            graph.AddEdge(3, 2);
            graph.AddEdge(5, 2);
            graph.AddEdge(6, 2);
            graph.AddEdge(4, 3);
            graph.AddEdge(6, 4);

            String[] lines = writer.Write(graph).Split(
            new String[] { "\r\n", "\r", "\n" },
            StringSplitOptions.None
            );

            Assert.That(lines[2], Is.EqualTo("\tc_2  [shape=circle, style=filled]"));
            Assert.That(lines[4], Is.EqualTo("\tc_4  [shape=circle, style=filled]"));
            Assert.That(lines[5], Is.EqualTo("\tc_5  [shape=circle, style=filled]"));
            Assert.That(lines[7], Is.EqualTo("\tc_2 -- {c_1};"));
            Assert.That(lines[9], Is.EqualTo("\tc_4 -- {c_3};"));
        }
        
        
    }
}
