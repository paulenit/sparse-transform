﻿using System;
using System.Linq;
using NUnit.Framework;
using SparseTransform.DataStructures;
using SparseTransform.Convert;

namespace SparseTransform.Tests.Library.Convert
{
    public class MatrixMarketReaderTest
    {
        private String bipartiteGraph = "%%MatrixMarket matrix coordinate real general\n" +
                                        "% -------------------------------------------------------------------------------\n" +
                                        "% UF Sparse Matrix Collection, Tim Davis\n" +
                                        "%-------------------------------------------------------------------------------\n" +
                                        "5 5 4\n" +
                                        "1 4 34\n" +
                                        "2 3 65\n" +
                                        "5 1 8\n" +
                                        "4 2 6\n" +
                                        "4 3 0\n" +
                                        "1 3 12\n" +
                                        "5 3 24";


        private String adjacencyGraph = "%%MatrixMarket matrix coordinate real symmetric\n" +
                                        "% -------------------------------------------------------------------------------\n" +
                                        "% UF Sparse Matrix Collection, Tim Davis\n" +
                                        "%-------------------------------------------------------------------------------\n" +
                                        "5 5 4\n" +
                                        "1 4 34\n" +
                                        "2 3 65\n" +
                                        "5 1 8\n" +
                                        "4 2 6\n" +
                                        "4 3 0\n" +
                                        "1 3 12\n" +
                                        "5 3 24";



        [Test]
        public void ReadGraphTestAdjacency()
        {
            MatrixMarketReader reader = new MatrixMarketReader();
            AdjacencyGraph graph = (AdjacencyGraph)reader.ReadGraph(adjacencyGraph);

            Assert.IsNotNull(graph);

            GraphNode[] nodes = Enumerable.ToArray(graph.GetNodes());

            Assert.AreEqual(5, nodes.Length);
            Assert.That(nodes[0].Index, Is.EqualTo(1));
            Assert.That(nodes[1].Index, Is.EqualTo(2));
            Assert.That(nodes[2].Index, Is.EqualTo(3));
            Assert.That(nodes[3].Index, Is.EqualTo(4));
            Assert.That(nodes[4].Index, Is.EqualTo(5));

            Assert.That(nodes[0].Neighbors.First().Index, Is.EqualTo(4));
            Assert.That(nodes[1].Neighbors.First().Index, Is.EqualTo(3));
            Assert.That(nodes[2].Neighbors.First().Index, Is.EqualTo(2));
            Assert.That(nodes[3].Neighbors.First().Index, Is.EqualTo(1));
            Assert.That(nodes[4].Neighbors.Last().Index, Is.EqualTo(3));
        }


        [Test]
        public void ReadGraphTestBipartite()
        {
            MatrixMarketReader reader = new MatrixMarketReader();
            BipartiteGraph graph = (BipartiteGraph)reader.ReadGraph(bipartiteGraph);

            Assert.IsNotNull(graph);

            GraphNode[] rightNodes = Enumerable.ToArray(graph.GetRightNodes());

            Assert.That(rightNodes.Length, Is.EqualTo(4));

            Assert.That(rightNodes[0].Index, Is.EqualTo(1));
            Assert.That(rightNodes[3].Index, Is.EqualTo(4));

            Assert.That(rightNodes[0].Neighbors.First().Index, Is.EqualTo(5));
            Assert.That(rightNodes[1].Neighbors.First().Index, Is.EqualTo(4));
            Assert.That(rightNodes[2].Neighbors.First().Index, Is.EqualTo(2));
            Assert.That(rightNodes[3].Neighbors.First().Index, Is.EqualTo(1));
        }
    }
}
