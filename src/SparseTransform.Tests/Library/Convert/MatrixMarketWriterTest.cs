﻿using System;
using NUnit.Framework;
using SparseTransform.DataStructures;
using SparseTransform.Convert;


namespace SparseTransform.Tests.Library.Convert
{
    public class MatrixMarketWriterTest
    {

        private String bipartiteGraph = "%%MatrixMarket matrix coordinate real general\n" +
                                        "% -------------------------------------------------------------------------------\n" +
                                        "% UF Sparse Matrix Collection, Tim Davis\n" +
                                        "%-------------------------------------------------------------------------------\n" +
                                        "5 5 4\n" +
                                        "1 4 34\n" +
                                        "2 3 65\n" +
                                        "5 1 8\n" +
                                        "4 2 6\n" +
                                        "4 3 0\n" +
                                        "1 3 12\n" +
                                        "5 3 24";


        private String adjacencyGraph = "%%MatrixMarket matrix coordinate real symmetric\n" +
                                        "% -------------------------------------------------------------------------------\n" +
                                        "% UF Sparse Matrix Collection, Tim Davis\n" +
                                        "%-------------------------------------------------------------------------------\n" +
                                        "5 5 4\n" +
                                        "1 4 34\n" +
                                        "2 3 65\n" +
                                        "5 1 8\n" +
                                        "4 2 6\n" +
                                        "4 3 0\n" +
                                        "1 3 12\n" +
                                        "5 3 24";


        [Test] 
        public void WriteTestBipartite()
        {
            MatrixMarketReader reader = new MatrixMarketReader();
            IGraph graph = reader.ReadGraph(bipartiteGraph);
            Colorer color = new Colorer();
            color.PartialD2Color((BipartiteGraph)graph);
            DoubleMatrix inputMatrix = reader.ReadMatrix(bipartiteGraph);
            MatrixMarketWriter writer = new MatrixMarketWriter(false, inputMatrix);

            String[] lines = writer.Write(graph).Split(
            new String[] { "\r\n", "\r", "\n" },
            StringSplitOptions.None
            );

            Assert.That(lines[0], Is.EqualTo("%%MatrixMarket matrix coordinate real general"));
            Assert.That(lines[2], Is.EqualTo("0 0 34"));
            Assert.That(lines[5], Is.EqualTo("3 0 6"));
            Assert.That(lines[6], Is.EqualTo("4 0 8"));
        }


        [Test]
        public void WriteTestAdjacency()
        {
            MatrixMarketReader reader = new MatrixMarketReader();
            IGraph graph = reader.ReadGraph(adjacencyGraph);
            Colorer color = new Colorer();
            color.StarColor1((AdjacencyGraph)graph);
            DoubleMatrix inputMatrix = reader.ReadMatrix(adjacencyGraph);
            MatrixMarketWriter writer = new MatrixMarketWriter(false, inputMatrix);

            String[] lines = writer.Write(graph).Split(
            new String[] { "\r\n", "\r", "\n" },
            StringSplitOptions.None
            );

            Assert.That(lines[0], Is.EqualTo("%%MatrixMarket matrix coordinate real general"));
            Assert.That(lines[2], Is.EqualTo("0 2 12"));
            Assert.That(lines[5], Is.EqualTo("3 1 6"));
            Assert.That(lines[6], Is.EqualTo("4 0 8"));
        }

    }
}
