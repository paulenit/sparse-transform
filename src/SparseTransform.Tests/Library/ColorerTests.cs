using NUnit.Framework;
using SparseTransform.DataStructures;
using System.Linq;

namespace SparseTransform.Tests.Library;

public class ColorerTests
{
    [Test]
    public void PartialD2ColorExampleFromPaperRightSideColored()
    {
        BipartiteGraph graph = new BipartiteGraph();
        graph.AddEdge(1, 1);
        graph.AddEdge(1, 2);
        graph.AddEdge(1, 5);
        graph.AddEdge(2, 3);
        graph.AddEdge(3, 2);
        graph.AddEdge(3, 3);
        graph.AddEdge(3, 4);
        graph.AddEdge(4, 1);
        graph.AddEdge(5, 4);
        graph.AddEdge(5, 5);
        Colorer colorer = new Colorer();
        colorer.PartialD2Color(graph);
        GraphNode[] rightNodes = Enumerable.ToArray(graph.GetRightNodes());
        Assert.That(rightNodes[0].Color, Is.EqualTo(1));
        Assert.That(rightNodes[1].Color, Is.EqualTo(2));
        Assert.That(rightNodes[2].Color, Is.EqualTo(1));
        Assert.That(rightNodes[3].Color, Is.EqualTo(3));
        Assert.That(rightNodes[4].Color, Is.EqualTo(4));
    }

    [Test]
    public void PartialD2ColorExampleFromPaperLeftSideUncolored()
    {
        BipartiteGraph graph = new BipartiteGraph();
        graph.AddEdge(1, 1);
        graph.AddEdge(1, 2);
        graph.AddEdge(1, 5);
        graph.AddEdge(2, 3);
        graph.AddEdge(3, 2);
        graph.AddEdge(3, 3);
        graph.AddEdge(3, 4);
        graph.AddEdge(4, 1);
        graph.AddEdge(5, 4);
        graph.AddEdge(5, 5);
        Colorer colorer = new Colorer();
        colorer.PartialD2Color(graph);
        foreach (GraphNode node in graph.GetLeftNodes())
        {
            Assert.That(node.Color, Is.EqualTo(0));
        }
    }

    [Test]
    public void PartialD2ColorExampleFromPaperGraphColoredFlagInitiallyFalse()
    {
        BipartiteGraph graph = new BipartiteGraph();
        graph.AddEdge(1, 1);
        Assert.That(graph.RightColored, Is.EqualTo(false));
    }

    [Test]
    public void PartialD2ColorExampleFromPaperGraphColoredFlagSet()
    {
        BipartiteGraph graph = new BipartiteGraph();
        graph.AddEdge(1, 1);
        Colorer colorer = new Colorer();
        colorer.PartialD2Color(graph);
        Assert.That(graph.RightColored, Is.EqualTo(true));
    }

    [Test]
    public void StarColor1Colored()
    {
        AdjacencyGraph graph = new AdjacencyGraph();
        graph.AddEdge(2, 1);
        graph.AddEdge(2, 5);
        graph.AddEdge(2, 6);
        graph.AddEdge(2, 3);
        graph.AddEdge(4, 6);
        graph.AddEdge(4, 3);
        Colorer colorer = new Colorer();
        colorer.StarColor1(graph);
        GraphNode[] nodes = Enumerable.ToArray(graph.GetNodes());
        Assert.That(nodes[0].Color, Is.EqualTo(nodes[2].Color));
        Assert.That(nodes[0].Color, Is.EqualTo(nodes[4].Color));
        Assert.That(nodes[0].Color, Is.EqualTo(nodes[5].Color));
        Assert.That(nodes[0].Color, Is.Not.EqualTo(nodes[1].Color));
        Assert.That(nodes[0].Color, Is.Not.EqualTo(nodes[3].Color));
        Assert.That(nodes[1].Color, Is.Not.EqualTo(nodes[3].Color));
    }

    [Test]
    public void StarColor1GraphColoredFlagInitiallyFalse()
    {
        AdjacencyGraph graph = new AdjacencyGraph();
        graph.AddEdge(1, 1);
        Assert.That(graph.Colored, Is.EqualTo(false));
    }

    [Test]
    public void StarColor1GraphColoredFlagSet()
    {
        AdjacencyGraph graph = new AdjacencyGraph();
        graph.AddEdge(1, 1);
        Colorer colorer = new Colorer();
        colorer.StarColor1(graph);
        Assert.That(graph.Colored, Is.EqualTo(true));
    }
}