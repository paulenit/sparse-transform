using NUnit.Framework;
using SparseTransform.DataStructures;
using System.Linq;

namespace SparseTransform.Tests.Library.DataStructures;

public class AdjacencyGraphTests
{
    [Test]
    public void AdjacencyGraphStartsEmpty()
    {
        AdjacencyGraph graph = new AdjacencyGraph();
        Assert.That(graph.NodeCount, Is.EqualTo(0));
    }

    [Test]
    public void AdjacencyGraphAddEdgeAllNewNodesCreated()
    {
        AdjacencyGraph graph = new AdjacencyGraph();
        graph.AddEdge(0, 1);
        Assert.That(graph.NodeCount, Is.EqualTo(2));
        GraphNode[] nodes = Enumerable.ToArray(graph.GetNodes());
        Assert.That(nodes[0], Is.Not.EqualTo(nodes[1]));
    }

    [Test]
    public void AdjacencyGraphAddEdgeOneNewNodeCreated()
    {
        AdjacencyGraph graph = new AdjacencyGraph();
        graph.AddEdge(0, 1);
        graph.AddEdge(0, 2);
        Assert.That(graph.NodeCount, Is.EqualTo(3));
    }

    [Test]
    public void AdjacencyGraphAddEdgeNoNewNodesCreated()
    {
        AdjacencyGraph graph = new AdjacencyGraph();
        graph.AddEdge(0, 1);
        graph.AddEdge(0, 2);
        graph.AddEdge(1, 2);
        Assert.That(graph.NodeCount, Is.EqualTo(3));
    }

    [Test]
    public void AdjacencyGraphNodesEnumerable()
    {
        AdjacencyGraph graph = new AdjacencyGraph();
        graph.AddEdge(0, 1);
        graph.AddEdge(0, 2);
        graph.AddEdge(3, 4);
        int counter = 0;
        foreach (GraphNode n in graph.GetNodes())
        {
            counter++;
        }
        Assert.That(counter, Is.EqualTo(5));
    }

    [Test]
    public void AdjacencyGraphMaxDegreeInitializes()
    {
        AdjacencyGraph graph = new AdjacencyGraph();
        graph.AddEdge(0, 1);
        Assert.That(graph.MaxDegree, Is.EqualTo(1));
    }

    [Test]
    public void AdjacencyGraphMaxDegreeUpdates()
    {
        AdjacencyGraph graph = new AdjacencyGraph();
        graph.AddEdge(0, 1);
        graph.AddEdge(0, 2);
        Assert.That(graph.MaxDegree, Is.EqualTo(2));
    }

    [Test]
    public void AdjacencyGraphSeedMatrixCorrectDimensions()
    {
        AdjacencyGraph graph = new AdjacencyGraph();
        graph.AddEdge(2, 1);
        graph.AddEdge(2, 5);
        graph.AddEdge(2, 6);
        graph.AddEdge(2, 3);
        graph.AddEdge(4, 6);
        graph.AddEdge(4, 3);
        Colorer colorer = new Colorer();
        colorer.StarColor1(graph);
        Matrix<int> seed = graph.ToSeedMatrix(6);
        // This should be the same as the n input to ToSeedMatrix
        Assert.That(seed.RowCount, Is.EqualTo(6));
        // This should be number of colors
        Assert.That(seed.ColumnCount, Is.EqualTo(3));
    }

    [Test]
    public void AdjacencyGraphSeedMatrixReturnsNullIfUncolored()
    {
        AdjacencyGraph graph = new AdjacencyGraph();
        graph.AddEdge(0, 1);
        Matrix<int>? seed = graph.ToSeedMatrix(1);
        Assert.That(seed, Is.Null);
    }
}