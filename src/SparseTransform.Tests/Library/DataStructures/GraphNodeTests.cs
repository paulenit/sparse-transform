using NUnit.Framework;
using SparseTransform.DataStructures;
using System;

namespace SparseTransform.Tests.Library.DataStructures;

public class GraphNodeTests
{
    [Test]
    public void GraphNodeStoresColor()
    {
        GraphNode node = new GraphNode(1);
        node.Color = 1;
        Assert.That(node.Color, Is.EqualTo(1));
    }

    [Test]
    public void GraphNodeIsColorableTrue()
    {
        GraphNode node = new GraphNode(1);
        node.Color = 1;
        Assert.That(node.Colored, Is.EqualTo(true));
    }

    [Test]
    public void GraphNodeIsColorableFalse()
    {
        GraphNode node = new GraphNode(1);
        Assert.That(node.Colored, Is.EqualTo(false));
    }

    [Test]
    public void GraphNodeColorIsImmutableAfterColoring()
    {
        GraphNode node = new GraphNode(1);
        node.Color = 1;
        Assert.Throws<ArgumentException>(() => { node.Color = 2; });
    }

    [Test]
    public void GraphNodeEdgesCanBeAdded()
    {
        GraphNode node = new GraphNode(1);
        GraphNode neighbor = new GraphNode(2);
        node.AddEdge(neighbor);
        Assert.That(node.Neighbors[0], Is.EqualTo(neighbor));
        Assert.That(node.Neighbors.Count, Is.EqualTo(1));
    }

    [Test]
    public void GraphNodeDegreeStartsAtZero()
    {
        GraphNode node = new GraphNode(1);
        Assert.That(node.Degree, Is.EqualTo(0));
    }

    [Test]
    public void GraphNodeDegreeIsUpdated()
    {
        GraphNode node = new GraphNode(1);
        GraphNode neighbor = new GraphNode(2);
        node.AddEdge(neighbor);
        Assert.That(node.Degree, Is.EqualTo(1));
    }
}