using NUnit.Framework;
using SparseTransform.DataStructures;
using System;

namespace SparseTransform.Tests.Library.DataStructures;

public class MatrixTests
{
    [Test]
    public void MatrixInitializedEmpty()
    {
        Matrix<double> matrix = new Matrix<double>(3, 3);
        for (int i = 0; i < matrix.RowCount; i++)
        {
            for (int j = 0; j < matrix.ColumnCount; j++)
            {
                Assert.That(matrix[i, j], Is.EqualTo(0));
            }
        }
    }

    [Test]
    public void MatrixElementIsSet()
    {
        Matrix<double> matrix = new Matrix<double>(3, 3);
        matrix[0, 1] = 1.0;
        Assert.That(matrix[0, 1], Is.EqualTo(1.0));
    }

    [Test]
    public void MatrixIsSymmetric()
    {
        Matrix<int> matrix = new Matrix<int>(3, 3);
        matrix[0, 0] = 1;
        matrix[0, 1] = 7;
        matrix[0, 2] = 3;
        matrix[1, 0] = 7;
        matrix[1, 1] = 4;
        matrix[1, 2] = 5;
        matrix[2, 0] = 3;
        matrix[2, 1] = 5;
        matrix[2, 2] = 0;
        Assert.That(matrix.Symmetric, Is.True);
    }

    [Test]
    public void MatrixIsNotSymmetric()
    {
        Matrix<int> matrix = new Matrix<int>(2, 2);
        matrix[0, 0] = 0;
        matrix[0, 1] = 1;
        matrix[1, 0] = 0;
        matrix[1, 1] = 1;
        Assert.That(matrix.Symmetric, Is.False);
    }
    
    [Test]
    public void MatrixIsNotSymmetricWithUnequalDimensions()
    {
        Matrix<int> matrix = new Matrix<int>(1, 2);
        Assert.That(matrix.Symmetric, Is.False);
    }
}