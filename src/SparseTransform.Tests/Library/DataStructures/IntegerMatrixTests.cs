using NUnit.Framework;
using SparseTransform.DataStructures;
using System.IO;

namespace SparseTransform.Tests.Library.DataStructures;

public class IntegerMatrixTests
{
    [Test]
    public void NonZerosCorrectNumber()
    {
        IntegerMatrix matrix = new IntegerMatrix(2, 2);
        matrix[0, 0] = 1;
        Assert.That(matrix.NonZeros, Is.EqualTo(1));
    }

    [Test]
    public void ToMatrixMarketStringCorrectHeaderSymmetric()
    {
        IntegerMatrix matrix = new IntegerMatrix(1, 1);
        using StringReader reader = new StringReader(matrix.ToMatrixMarketString());
        Assert.That(reader.ReadLine(), Is.EqualTo("%%MatrixMarket matrix coordinate integer symmetric"));
    }

    [Test]
    public void ToMatrixMarketStringCorrectHeaderNotSymmetric()
    {
        IntegerMatrix matrix = new IntegerMatrix(1, 2);
        using StringReader reader = new StringReader(matrix.ToMatrixMarketString());
        Assert.That(reader.ReadLine(), Is.EqualTo("%%MatrixMarket matrix coordinate integer general"));
    }

    [Test]
    public void ToMatrixMarketStringCorrectDimensions()
    {
        IntegerMatrix matrix = new IntegerMatrix(2, 2);
        matrix[0, 1] = 1;
        matrix[1, 1] = 2;
        using StringReader reader = new StringReader(matrix.ToMatrixMarketString());
        reader.ReadLine();
        Assert.That(reader.ReadLine(), Is.EqualTo("2 2 2"));
    }

    [Test]
    public void ToMatrixMarketStringCorrectContent()
    {
        IntegerMatrix matrix = new IntegerMatrix(2, 2);
        matrix[0, 1] = 1;
        matrix[1, 1] = 2;
        using StringReader reader = new StringReader(matrix.ToMatrixMarketString());
        reader.ReadLine();
        reader.ReadLine();
        Assert.That(reader.ReadLine(), Is.EqualTo("0 1 1"));
        Assert.That(reader.ReadLine(), Is.EqualTo("1 1 2"));
    }
}