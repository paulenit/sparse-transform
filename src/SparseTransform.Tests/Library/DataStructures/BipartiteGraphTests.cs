using NUnit.Framework;
using SparseTransform.DataStructures;

namespace SparseTransform.Tests.Library.DataStructures;

public class BipartiteGraphTests
{
    [Test]
    public void BipartiteGraphStartsEmpty()
    {
        BipartiteGraph graph = new BipartiteGraph();
        Assert.That(graph.LeftNodeCount, Is.EqualTo(0));
        Assert.That(graph.RightNodeCount, Is.EqualTo(0));
    }

    [Test]
    public void BipartiteGraphAddEdgeAllNewNodesCreated()
    {
        BipartiteGraph graph = new BipartiteGraph();
        graph.AddEdge(0, 1);
        Assert.That(graph.LeftNodeCount, Is.EqualTo(1));
        Assert.That(graph.RightNodeCount, Is.EqualTo(1));
    }

    [Test]
    public void BipartiteGraphAddEdgeOneNewNodeCreated()
    {
        BipartiteGraph graph = new BipartiteGraph();
        graph.AddEdge(0, 1);
        graph.AddEdge(0, 2);
        Assert.That(graph.LeftNodeCount, Is.EqualTo(1));
        Assert.That(graph.RightNodeCount, Is.EqualTo(2));
    }

    [Test]
    public void BipartiteGraphAddEdgeNoNewNodesCreated()
    {
        BipartiteGraph graph = new BipartiteGraph();
        graph.AddEdge(0, 1);
        graph.AddEdge(0, 2);
        graph.AddEdge(1, 2);
        Assert.That(graph.LeftNodeCount, Is.EqualTo(2));
        Assert.That(graph.RightNodeCount, Is.EqualTo(2));
    }

    [Test]
    public void BipartiteGraphNodesEnumerable()
    {
        BipartiteGraph graph = new BipartiteGraph();
        graph.AddEdge(0, 1);
        graph.AddEdge(0, 2);
        graph.AddEdge(3, 4);
        int leftCounter = 0;
        foreach (GraphNode n in graph.GetLeftNodes())
        {
            leftCounter++;
        }
        int rightCounter = 0;
        foreach (GraphNode n in graph.GetRightNodes())
        {
            rightCounter++;
        }
        Assert.That(leftCounter, Is.EqualTo(2));
        Assert.That(rightCounter, Is.EqualTo(3));
    }

    [Test]
    public void BipartiteGraphMaxDegreeInitializes()
    {
        BipartiteGraph graph = new BipartiteGraph();
        graph.AddEdge(0, 1);
        Assert.That(graph.MaxDegreeRight, Is.EqualTo(1));
        Assert.That(graph.MaxDegreeLeft, Is.EqualTo(1));
    }

    [Test]
    public void BipartiteGraphMaxDegreeUpdates()
    {
        BipartiteGraph graph = new BipartiteGraph();
        graph.AddEdge(0, 1);
        graph.AddEdge(0, 2);
        Assert.That(graph.MaxDegreeLeft, Is.EqualTo(2));
        Assert.That(graph.MaxDegreeRight, Is.EqualTo(1));
    }


    [Test]
    public void BipartiteGraphSeedMatrixCorrectDimensions()
    {
        BipartiteGraph graph = new BipartiteGraph();
        graph.AddEdge(1, 1);
        graph.AddEdge(1, 2);
        graph.AddEdge(1, 5);
        graph.AddEdge(2, 3);
        graph.AddEdge(3, 2);
        graph.AddEdge(3, 3);
        graph.AddEdge(3, 4);
        graph.AddEdge(4, 1);
        graph.AddEdge(5, 4);
        graph.AddEdge(5, 5);
        Colorer colorer = new Colorer();
        colorer.PartialD2Color(graph);
        Matrix<int> seed = graph.ToSeedMatrix(5);
        // This should be the same as the n input to ToSeedMatrix
        Assert.That(seed.RowCount, Is.EqualTo(5));
        // This should be number of colors
        Assert.That(seed.ColumnCount, Is.EqualTo(4));
    }

    [Test]
    public void BipartiteGraphSeedMatrixReturnsNullIfUncolored()
    {
        BipartiteGraph graph = new BipartiteGraph();
        graph.AddEdge(1, 1);
        Matrix<int> seed = graph.ToSeedMatrix(5);
        Assert.That(seed, Is.Null);
    }
}