using SparseTransform.DataStructures;

namespace SparseTransform
{
    /// <summary>
    /// Colorer class. Contains implementation for all available coloring algorithms.
    /// All algorithms are in-place.
    /// </summary>
    public class Colorer
    {
        /// <summary>
        /// Colors the passed bipartite graph in-place using partial distance-2 coloring.
        /// </summary>
        /// <param name="graph">Graph to color</param>
        /// <returns><c>true</c> if coloring was successfull</returns>
        public bool PartialD2Color(BipartiteGraph graph)
        {
            // Exact length defined in paper
            int[] forbiddenColors = new int[Math.Min(graph.MaxDegreeRight * (graph.MaxDegreeLeft - 1) + 1, graph.RightNodeCount)];
            // Make sure the default values are neither (1) potential colors (2) the uncolored value (=0)
            Array.Fill(forbiddenColors, -1);
            foreach (GraphNode v_i in graph.GetRightNodes())
            {
                foreach (GraphNode w in v_i.Neighbors)
                {
                    foreach (GraphNode x in w.Neighbors)
                    {
                        if (x.Colored)
                        {
                            forbiddenColors[x.Color - 1] = v_i.Index;
                        }
                    }
                }
                int c = 1;
                while (c > 0)
                {
                    if (forbiddenColors[c - 1] != v_i.Index)
                    {
                        v_i.Color = c;
                        c = 0;
                    }
                    else
                    {
                        c++;
                    }
                }
            }
            // Set right since we performed column partitioning
            graph.RightColored = true;
            return true;
        }

        /// <summary>
        /// Colors the passed adjacency graph in-place using the first star coloring alg from https://doi.org/10.1137/S0036144504444711
        /// </summary>
        /// <param name="graph">Graph to color</param>
        /// <returns><c>true</c> if coloring was successfull</returns>
        public bool StarColor1(AdjacencyGraph graph)
        {
            // Exact length defined in paper
            int[] forbiddenColors = new int[Math.Min(graph.MaxDegree * graph.MaxDegree + 1, graph.NodeCount)];
            // Make sure the default values are neither (1) potential colors (2) the uncolored value (=0)
            Array.Fill(forbiddenColors, -1);
            foreach (GraphNode v_i in graph.GetNodes())
            {
                foreach (GraphNode w in v_i.Neighbors)
                {
                    if (w.Colored)
                    {
                        forbiddenColors[w.Color - 1] = v_i.Index;
                    }
                    foreach (GraphNode x in w.Neighbors)
                    {
                        if (x.Colored)
                        {
                            if (!w.Colored)
                            {
                                forbiddenColors[x.Color - 1] = v_i.Index;
                            }
                            else
                            {
                                foreach (GraphNode y in x.Neighbors)
                                {
                                    if (y.Colored && y != w)
                                    {
                                        if (y.Color == w.Color)
                                        {
                                            forbiddenColors[x.Color - 1] = v_i.Index;
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                int c = 1;
                while (c > 0)
                {
                    if (forbiddenColors[c - 1] != v_i.Index)
                    {
                        v_i.Color = c;
                        c = 0;
                    }
                    else
                    {
                        c++;
                    }
                }
            }
            graph.Colored = true;
            return true;
        }
    }
}