﻿using SparseTransform.DataStructures;

namespace SparseTransform.Convert
{
    /// <summary>
    /// Common interface for reader classes that transform a matrix input string into data structures from SparseTransform.DataStructures
    /// </summary>
    public interface IReader
    {
        /// <summary>
        /// Transforms the <c>matrix</c> to an IGraph (respectively a Bipartite- or AdjacencyGraph)
        /// </summary>
        /// <param name="matrix"></param>
        /// <returns>transformed graph <c>IGraph</c></returns>
        public IGraph ReadGraph(String matrix);

        /// <summary>
        /// Transform the input matrix String into a matrix data structure.
        /// This is needed since information from the input matrix is required for some conversions.
        /// </summary>
        /// <param name="matrix">matrix string to transform</param>
        /// <returns>matrix object</returns>
        public DoubleMatrix ReadMatrix(String matrix);
    }
}
