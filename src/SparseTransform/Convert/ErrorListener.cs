﻿using Antlr4.Runtime;
using Antlr4.Runtime.Misc;

namespace SparseTransform.Convert
{
    public class EListener : IAntlrErrorListener<IToken>
    {
        /// <summary>
        /// specialization for parsing error handling
        /// </summary>
        /// <param name="recognizer"></param>
        /// <param name="offendingSymbol"></param>
        /// <param name="line"></param>
        /// <param name="charPositionInLine"></param>
        /// <param name="msg">thrown error message</param>
        /// <param name="e"></param>
        /// <exception cref="Exception"></exception>
        public void SyntaxError([NotNull] IRecognizer recognizer, [Nullable] IToken offendingSymbol, int line, int charPositionInLine, [NotNull] String msg, [Nullable] RecognitionException e)
        {
            throw new Exception("Error in MatrixMarket header line: " + msg);
        }
    }
}

