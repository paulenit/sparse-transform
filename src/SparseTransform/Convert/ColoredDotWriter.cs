﻿using SparseTransform.DataStructures;
using System.Text;

namespace SparseTransform.Convert
{
    /// <summary>
    /// Extends the DotWriter class to output a colored DOT graph.
    /// </summary>
    public class ColoredDotWriter : DotWriter
    {
        private bool _textcolor;
        private int _colorsUsed;
        private int _hueFactor;

        /// <summary>
        /// Constructor constructing a color-dictionary for coloring purpose.
        /// </summary>
        /// <param name="textColor">decision whether the labels should be colored or labelled.</param>
        public ColoredDotWriter(IGraph graph, bool textColor)
        {
            this._colorsUsed = graph.ColorsUsed;
            this._textcolor = textColor;
            // This is the offset for coloring. Less colors = more distance between. Cached for NodeLabel.
            this._hueFactor = 255 / this._colorsUsed;
        }
       
        /// <summary>
        /// Labels the node with the respective colors determined by it's index. Overrides the method in DotWriter 
        /// </summary>
        /// <param name="node">the node to be labeld</param>
        /// <param name="prefix">the paefix of the node-name (either 'c_' or 'r_'</param>
        /// <returns></returns>
        public override String NodeLabel(GraphNode node, String prefix)
        {
            // Calculate the hue. We modulo with 255 to not exceed spectrum. Then convert to float for DOT
            double hue = Math.Round((double)((node.Color * _hueFactor) % 255) / 255, 3);

            StringBuilder label = new StringBuilder();
            // Append label name and general layout info
            label.Append($"\t{prefix}{node.Index} [shape=circle, style=filled");

            // If current node is colored, apply either text label with color or set fill color
            if (node.Colored)
            {
                if (_textcolor || this._colorsUsed > 255)
                {
                    label.Append($", label=\"{prefix}{node.Index}\\n color:{node.Color}\"");
                }
                else
                {
                    // Set saturation + value (brightness) to max for vibrant colors
                    label.Append($", fillcolor=\"{hue} {1} {1}\"");
                }
            }

            // Close attribute line
            label.Append("]");
            return label.ToString();
        }
    }
}
