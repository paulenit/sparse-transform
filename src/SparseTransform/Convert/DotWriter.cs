﻿using System.Text;
using SparseTransform.DataStructures;

namespace SparseTransform.Convert
{
    /// <summary>
    /// Provides an IWriter implementation that outputs an uncolored DOT graph.
    /// </summary>
    public class DotWriter : IWriter
    {
        /// <summary>
        /// Transforms the <c>graph</c> to a DOT string.
        /// </summary>
        /// <param name="graph">IGraph implementation to transform</param>
        /// <returns>DOT string</returns>
        public String Write(IGraph graph)
        {
            StringBuilder output = new StringBuilder();

            if (graph is AdjacencyGraph)
            {
                return processAdjacencyGraph((AdjacencyGraph)graph, output).ToString();
            }
            else
            {
                return processBipartiteGraph((BipartiteGraph)graph, output).ToString();
            }
        }

        /// <summary>
        /// generates and adds a description-line in dot-format for every node and edge in the <c>inputGraph</c>
        /// while interprating the graph as a adjacency graph
        /// </summary>
        /// <param name="inputGraph">input adjacency graph</param>
        /// <param name="output"></param>
        /// <returns>dot-formatted string representing the <c>inputGraph</c></returns>
        private StringBuilder processAdjacencyGraph(AdjacencyGraph inputGraph, StringBuilder output)
        {
            output.AppendLine("graph G {");
            processNodes(inputGraph.GetNodes(), output, false);
            processEdges(inputGraph.GetNodes(), output, false);
            output.AppendLine("}");
            return output;
        }

        /// <summary>
        /// generates and adds a description-line in dot-format for every node and edge in the <c>inputGraph</c> 
        /// while interprating the graph as a bipartite graph
        /// 
        /// </summary>
        /// <param name="inputGraph">input bipartite graph</param>
        /// <param name="output"></param>
        /// <returns>dot-formatted string representing the <c>inputGraph</c></returns>
        private StringBuilder processBipartiteGraph(BipartiteGraph inputGraph, StringBuilder output)
        {
            output.AppendLine("graph G {");
            output.AppendLine("\tforcelabels=true;");
            processNodes(inputGraph.GetLeftNodes(), output, true);
            processNodes(inputGraph.GetRightNodes(), output, false);
            processEdges(inputGraph.GetRightNodes(), output, true);
            output.AppendLine("}");
            return output;
        }

        /// <summary>
        /// creates an edge entry in dot-format for each node in list <c>nodes</c> 
        /// </summary>
        /// <param name="nodes"></param>
        /// <param name="output"></param>
        /// <param name="bipartite"></param>
        private void processEdges(IEnumerable<GraphNode> nodes, StringBuilder output, bool bipartite)
        {
            String edge = "";
            foreach (GraphNode v in nodes)
            {
                edge = "";
                foreach (GraphNode w in v.Neighbors)
                {
                    if (w.Index <= v.Index || bipartite)
                    {
                        edge = edge + (bipartite ? "r_" : "c_") + w.Index + " ";
                    }
                }

                if (edge != "")
                {
                    edge = "\t" + "c_" + v.Index + " -- {" + edge.Substring(0, edge.Length - 1) + "};";
                    output.AppendLine(edge);
                }
            }
        }

        /// <summary>
        /// creates a node entry in dot-format for each node in list <c>nodes</c> 
        /// </summary>
        /// <param name="nodes"></param>
        /// <param name="output"></param>
        /// <param name="bipartite"></param>
        private void processNodes(IEnumerable<GraphNode> nodes, StringBuilder output, bool left)
        {
            foreach (GraphNode v in nodes)
            {
                output.AppendLine(NodeLabel(v, (left ? "r_" : "c_")));
            }
        }

        /// <summary>
        /// auxiliary method for creating the dot-node label of graph node
        /// </summary>
        /// <param name="node"></param>
        /// <param name="prefix">name of the node in the corresponding label</param>
        /// <returns></returns>
        public virtual String NodeLabel(GraphNode node, String prefix)
        {
            return "\t" + prefix + node.Index + "  [shape=circle, style=filled]";
        }
    }
}
