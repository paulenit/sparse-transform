﻿using SparseTransform.DataStructures;

namespace SparseTransform.Convert
{
    /// <summary>
    /// Common interface for writer classes that transform a graph object to a serialized string.
    /// </summary>
    public interface IWriter
    {
        /// <summary>
        /// Transforms the <c>graph</c> to a string representation.
        /// The format of String is determined by the implementing class and may not necessarily represent a graph.
        /// </summary>
        /// <param name="graph">IGraph implementation to transform</param>
        /// <returns>transformed string</returns>
        public String Write(IGraph graph);


    }
}
