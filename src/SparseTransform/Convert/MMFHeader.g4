﻿grammar MMFHeader;


header : PERCENT PERCENT MATRIXMARKET MATRIX format;

format : choice1 | choice2 | choice3;

choice1 : ( COORDINATE | ARRAY)  (REAL | INTEGER | COMPLEX)  (GENERAL | SYMMETRIC | SKEW);

choice2 : ( COORDINATE | ARRAY)  COMPLEX  HERMITIAN;

choice3 : COORDINATE  PATTERN  (GENERAL | SYMMETRIC);

PERCENT : '%';
MATRIXMARKET : 'MatrixMarket';
MATRIX : 'matrix';
COORDINATE : 'coordinate';
ARRAY : 'array';
REAL : 'real';
INTEGER : 'integer';
COMPLEX : 'complex';
GENERAL : 'general';
SYMMETRIC : 'symmetric';
SKEW : 'skew-symmetric';
HERMITIAN : 'Hermitian';
PATTERN : 'pattern';