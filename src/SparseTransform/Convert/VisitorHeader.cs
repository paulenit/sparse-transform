﻿using Antlr4.Runtime.Misc;
using SparseTransform.Convert;

namespace SparseTransform.Convert
{
    public class VisitorHeader : MMFHeaderBaseVisitor<bool>
    {


        /// <summary>
        /// visits the AST and helps to decide whether the input matrix is symmetric
        /// </summary>
        /// <param name="context"></param>
        /// <returns>true if the input matrix is not symmetric</returns>
        public override bool VisitHeader([NotNull] MMFHeaderParser.HeaderContext context)
        {
            if ((context.format().choice1() is null || context.format().choice1().GENERAL() is null)
                && (context.format().choice3() is null || context.format().choice3().GENERAL() is null))
            {
                return false;
            }
             else
            {
                return true;
            }
        }
    }
}
