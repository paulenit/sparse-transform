﻿using SparseTransform.DataStructures;

namespace SparseTransform.Convert
{
    /// <summary>
    /// Provides an IWriter implementation that outputs a MatrixMarket string.
    /// Supports output of both the seed matrix and compressed matrix.
    /// </summary>
    public class MatrixMarketWriter : IWriter
    {
        /// <summary>
        /// If true, write (only) the seed matrix
        /// </summary>
        private bool _seed;

        /// <summary>
        /// Input matrix that was read in using an IReader implementation.
        /// Used for computing the compressed matrix.
        /// </summary>
        private DoubleMatrix _inputMatrix;

        /// <summary>
        /// Construct a new MatrixMarketWriter.
        /// </summary>
        /// <param name="seed">if true, output seed matrix</param>
        /// <param name="inputMatrix">input matrix</param>
        public MatrixMarketWriter(bool seed, DoubleMatrix inputMatrix)
        {
            this._inputMatrix = inputMatrix;
            this._seed = seed;
        }

        /// <summary>
        /// Transforms the <c>graph</c> to a MatrixMarket string.
        /// If seed flag was set in constructor, the seed matrix is written.
        /// </summary>
        /// <param name="graph">IGraph implementation to transform</param>
        /// <returns>MatrixMarket string</returns>
        public String Write(IGraph graph)
        {
            IntegerMatrix? seedMatrix = graph.ToSeedMatrix(_inputMatrix.ColumnCount);
            if (_seed)
            {
                if (seedMatrix is IntegerMatrix && seedMatrix.ColumnCount != 0)
                {
                    return seedMatrix.ToMatrixMarketString();
                }
                return "null";
            }
            else
            {
                if (seedMatrix is IntegerMatrix && _inputMatrix is DoubleMatrix && seedMatrix.ColumnCount != 0)
                {
                    return CalculateCompressedMatrix(_inputMatrix, seedMatrix).ToMatrixMarketString();
                }
                return "null";
            }
        }

        /// <summary>
        /// Calculate the compressed matrix from the input and seed matrix.
        /// The formula is A*S = B where A is the input and B the compressed matrix.
        /// </summary>
        private DoubleMatrix CalculateCompressedMatrix(DoubleMatrix inputMatrix, IntegerMatrix seedMatrix)
        {
            return inputMatrix.Multiply(seedMatrix);
        }
    }
}