﻿﻿using SparseTransform.DataStructures;
using SparseTransform.Convert;

namespace SparseTransform
{
    public class ConversionManager
    {

        /// <summary>
        /// Converts Matrix from MatrixMarket format in Dot
        /// </summary>
        /// <param name="MMMatrix">raw MatrixMarket input String</param>
        /// <returns>Dot String </returns>
        public static String Convert2Dot(String MMMatrix)
        {
            MatrixMarketReader reader = new MatrixMarketReader();
            IGraph graph = reader.ReadGraph(MMMatrix);
            DotWriter writer = new DotWriter();
            return writer.Write(graph);
        }


        /// <summary>
        /// / Partitions as SparseMatrix using graph coloring
        /// </summary>
        /// <param name="MMMatrix">raw MatrixMarket input String</param> 
        /// <param name="textColor">true if color should additionally be represented as text</param>
        /// <returns>Dot String with coloring information</returns>
        public static String Partition2ColoredDot(String MMMatrix, bool textColor = false)
        {
            MatrixMarketReader reader = new MatrixMarketReader();
            IGraph graph = reader.ReadGraph(MMMatrix);
            Colorer color = new Colorer();
            if (graph is AdjacencyGraph)
            {
               
                color.StarColor1((AdjacencyGraph) graph);


            }
            else
                color.PartialD2Color((BipartiteGraph)graph);
            ColoredDotWriter writer = new ColoredDotWriter(graph, textColor);
            return writer.Write(graph);

        }
        /// <summary>
        /// Partions SparseMatrix in Seed Matrix
        /// </summary>
        /// <param name="MMMatrix">raw MatrixMarket input String</param>
        /// <returns>Seed Matrix as String</returns>
        public static String Partition2SeedMatrix(String MMMatrix)
        {
            MatrixMarketReader reader = new MatrixMarketReader();
            IGraph graph = reader.ReadGraph(MMMatrix);
            Colorer color = new Colorer();
            if (graph is AdjacencyGraph)
            {

                color.StarColor1((AdjacencyGraph)graph);


            }
            else
                color.PartialD2Color((BipartiteGraph)graph);
            DoubleMatrix inputMatrix = reader.ReadMatrix(MMMatrix);
            MatrixMarketWriter writer = new MatrixMarketWriter(true, inputMatrix);
            return writer.Write(graph);
        }
        /// <summary>
        /// Partitions SparseMatricx in CompressedMatrix
        /// </summary>
        /// <param name="MMMatrix">raw MatrixMarket input String</param>
        /// <returns>Compressed Matrix as String</returns>
        public static String Partition2CompressedMatrix(String MMMatrix)
        {
            MatrixMarketReader reader = new MatrixMarketReader();
            IGraph graph = reader.ReadGraph(MMMatrix);
            Colorer color = new Colorer();
            if (graph is AdjacencyGraph)
            {

                color.StarColor1((AdjacencyGraph)graph);


            }
            else
                color.PartialD2Color((BipartiteGraph)graph);
            DoubleMatrix inputMatrix = reader.ReadMatrix(MMMatrix);
            MatrixMarketWriter writer = new MatrixMarketWriter(false, inputMatrix);
            return writer.Write(graph);
        }
    }
}

