namespace SparseTransform.DataStructures
{
    /// <summary>
    /// General class for representing sparse matrices as graphs.
    /// BipartiteGraphs can be used to represent every sparse matrix but we only model unsymmetric ones with it.
    /// Uses left and right as abstractions for row and column indices.
    /// This is the same naming convention that ColPack follows.
    /// </summary>
    public class BipartiteGraph : IGraph
    {
        /// <summary>
        /// Stores nodes representing rows in the matrix representation.
        /// </summary>
        /// <remarks>
        /// Uses "left" in the name since ColPack does it too and in visualizations this is where the row nodes are often pictured.
        /// </remarks>
        private Dictionary<int, GraphNode> _leftNodes;

        /// <summary>
        /// Stores nodes representing columns in the matrix representation.
        /// </summary>
        /// <remarks>
        /// Uses "right" in the name since ColPack does this too and in visualizations this is where the row nodes are often pictured.
        /// </remarks>
        private Dictionary<int, GraphNode> _rightNodes;

        /// <summary>
        /// Number of nodes on the left side
        /// </summary>
        public int LeftNodeCount
        {
            get
            {
                return _leftNodes.Count;
            }
        }

        /// <summary>
        /// Number of nodes on the right side
        /// </summary>
        public int RightNodeCount
        {
            get
            {
                return _rightNodes.Count;
            }
        }

        private int _maxDegreeLeft = 0;
        /// <summary>
        /// Get the degree of the node on the left side with most neighbors.
        /// </summary>
        public int MaxDegreeLeft
        { get => _maxDegreeLeft; }

        private int _maxDegreeRight = 0;
        /// <summary>
        /// Get the degree of the node on the right side with most neighbors.
        /// </summary>
        public int MaxDegreeRight
        { get => _maxDegreeRight; }

        private bool _leftColored = false;
        /// <summary>
        /// True if left side is colored. To be set by Colorer. Cannot be unset.
        /// </summary>
        public bool LeftColored
        {
            get => _leftColored;
            set
            {
                if (_leftColored == false)
                    _leftColored = true;
            }
        }

        private bool _rightColored = false;
        /// <summary>
        /// True if left side is colored. To be set by Colorer. Cannot be unset.
        /// </summary>
        public bool RightColored
        {
            get => _rightColored;
            set
            {
                if (_rightColored == false)
                    _rightColored = true;
            }
        }

        private int _colorsUsed = 0;
        /// <summary>
        /// Number of colors used for coloring. Works if either or both sides are colored
        /// </summary>
        public int ColorsUsed
        {
            get
            {
                if (_colorsUsed == 0)
                {
                    List<int> colors = new List<int>();
                    if (LeftColored)
                    {
                        foreach (GraphNode node in _leftNodes.Values)
                        {
                            int color = node.Color;
                            // Do not check for != 0 since we already know everything here is colored
                            if (!colors.Contains(color))
                            {
                                colors.Add(color);
                            }
                        }
                    }
                    if (RightColored)
                    {
                        foreach (GraphNode node in _rightNodes.Values)
                        {
                            int color = node.Color;
                            // Do not check for != 0 since we already know everything here is colored
                            if (!colors.Contains(color))
                            {
                                colors.Add(color);
                            }
                        }
                    }
                    this._colorsUsed = colors.Count;
                }

                return _colorsUsed;

            }
        }

        /// <summary>
        /// Initialize bipartite graph with no nodes.
        /// </summary>
        public BipartiteGraph()
        {
            _leftNodes = new Dictionary<int, GraphNode>();
            _rightNodes = new Dictionary<int, GraphNode>();
        }

        /// <summary>
        /// Adds an edge between nodes i and j. If either of the nodes is missing it is created.
        /// This method expects i to be on the left and j to be on the right side.
        /// </summary>
        /// <param name="i">first node. Added to left side.</param>
        /// <param name="j">second node. Added to right side.</param>
        public void AddEdge(int i, int j)
        {
            GraphNode iNode = AddLeftNode(i);
            GraphNode jNode = AddRightNode(j);
            iNode.AddEdge(jNode);
            jNode.AddEdge(iNode);
            // Update degree
            if (iNode.Degree > MaxDegreeLeft)
                _maxDegreeLeft = iNode.Degree;
            if (jNode.Degree > MaxDegreeRight)
                _maxDegreeRight = jNode.Degree;
        }

        /// <summary>
        /// Adds a node to the left side
        /// </summary>
        /// <param name="index">index to create node for</param>
        private GraphNode AddLeftNode(int index)
        {
            if(!_leftNodes.ContainsKey(index))
            {
                GraphNode n = new GraphNode(index);
                _leftNodes[index] = n;
                return n;
            }
            else
            {
                return _leftNodes[index];
            }
        }

        /// <summary>
        /// Adds a node to the right side
        /// </summary>
        /// <param name="index">index to create node for</param>
        private GraphNode AddRightNode(int index)
        {
            if(!_rightNodes.ContainsKey(index))
            {
                GraphNode n = new GraphNode(index);
                _rightNodes[index] = n;
                return n;
            }
            else
            {
                return _rightNodes[index];
            }
        }

        /// <summary>
        /// Returns nodes on the left side sorted by index in ascending order as IEnumerable
        /// </summary>
        /// <returns>IEnumerable for the left nodes</returns>
        public IEnumerable<GraphNode> GetLeftNodes()
        {
            List<GraphNode> leftNodes = new List<GraphNode>(_leftNodes.Values);
            leftNodes.Sort((x, y) =>
            {
                if (x.Index < y.Index)
                    return -1;
                else
                    return 1;
            });
            return leftNodes as IEnumerable<GraphNode>;
        }

        /// <summary>
        /// Returns nodes on the right side sorted by index in ascending order as IEnumerable
        /// </summary>
        /// <returns>IEnumerable for the right nodes</returns>
        public IEnumerable<GraphNode> GetRightNodes()
        {
            List<GraphNode> rightNodes = new List<GraphNode>(_rightNodes.Values);
            rightNodes.Sort((x, y) =>
            {
                if (x.Index < y.Index)
                    return -1;
                else
                    return 1;
            });
            return rightNodes as IEnumerable<GraphNode>;
        }

        /// <summary>
        /// Builds a seed matrix from the coloring info of this graph.
        /// Requires one colored side. If neither is colored this method returns null
        /// </summary>
        /// <param name="rows">Rows of the seed matrix. Equal to the rows of the input matrix</param>
        /// <returns>seed matrix if either side was colored, null otherwise</returns>
        public IntegerMatrix? ToSeedMatrix(int rows)
        {
            IntegerMatrix seed = new IntegerMatrix(rows, ColorsUsed);
            // This only works for row OR column partitions not mixed
            if (LeftColored)
            {
                foreach (GraphNode node in _leftNodes.Values)
                {
                    // The row index is the column index of the element
                    // The column index = binary vector index is the color
                    // Result: Every column of the seed matrix corresponds to one color
                    seed[node.Index - 1, node.Color - 1] = 1;
                }
                return seed;
            }
            else if (RightColored)
            {
                foreach (GraphNode node in _rightNodes.Values)
                {
                    // The row index is the row index of the element
                    seed[node.Index - 1, node.Color - 1] = 1;
                }
                return seed;
            }
            else
            {
                // If neither side is colored a null matrix is returned
                return null;
            }
        }
    }
}