namespace SparseTransform.DataStructures
{
    public class GraphNode
    {
        /// <summary>
        /// This node's unique identifier in a given graph.
        /// </summary>
        /// <remarks>
        /// Depending on the implementation this will be a row or column index.
        /// </remarks>
        public int Index
        { get; }

        /// <summary>
        /// List of <see cref="GraphNode"/>'s this is adjacent to
        /// </summary>
        public List<GraphNode> Neighbors
        { get; }

        private int _color;
        /// <summary>
        /// Color identifier.
        /// </summary>
        /// <remarks>
        /// 0 corresponds to no color.
        /// </remarks>
        public int Color
        {
            get => _color;
            set
            {
                if (_color == 0)
                {
                    _color = value;
                }
                else
                {
                    throw new ArgumentException("Coloring is immutable! Once colored, this value may not be changed");
                }
            }
        }

        /// <summary>
        /// Wether this node is colored.
        /// </summary>
        /// <returns>true if this GraphNode is colored</returns>
        public bool Colored
        {
            get { return _color != 0; }
        }

        /// <summary>
        /// The mathematical degree of this node
        /// </summary>
        public int Degree
        {
            get
            {
                return Neighbors.Count;
            }
        }

        /// <summary>
        /// Construct a new GraphNode with a given index.
        /// Does not contain neighbors initially.
        /// </summary>
        /// <param name="index"></param>
        public GraphNode(int index)
        {
            Index = index;
            Neighbors = new List<GraphNode>();
            _color = 0;
        }

        /// <summary>
        /// Add edge to the <c>target</c> node. <c>target</c> must not be <c>null</c>.
        /// </summary>
        /// <param name="target"></param>
        public void AddEdge(GraphNode target)
        {
            Neighbors.Add(target);
        }
    }
}