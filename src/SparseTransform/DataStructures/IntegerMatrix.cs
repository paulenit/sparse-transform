using System.Text;

namespace SparseTransform.DataStructures
{
    /// <summary>
    /// Matrix type that can only store Integers.
    /// Provides methods that require int's numerical properties such as a zero element and algebra.
    /// </summary>
    public class IntegerMatrix : Matrix<int>
    {
        /// <summary>
        /// Return the number of non-zero elements in the matrix.
        /// </summary>
        public int NonZeros
        {
            get
            {
                int nonZeros = 0;
                for (int i = 0; i < RowCount; i++)
                {
                    for (int j = 0; j < ColumnCount; j++)
                    {
                        if (this[i, j] != 0)
                        {
                            nonZeros++;
                        }
                    }
                }
                return nonZeros;
            }
        }

        /// <summary>
        /// Initialize a zero matrix with the given dimensions
        /// </summary>
        /// <param name="rows">number of rows in matrix</param>
        /// <param name="columns">number of columns in matrix</param>
        public IntegerMatrix(int rows, int columns) : base(rows, columns) { }

        /// <summary>
        /// Build MatrixMarket String from matrix data. See <see href="https://math.nist.gov/MatrixMarket/formats.html">MatrixMarket site for format specification</see>
        /// </summary>
        /// <returns>MatrixMarket String</returns>
        public String ToMatrixMarketString()
        {
            StringBuilder MMString = new StringBuilder();
            String type = "integer";
            String symmetry;
            if (Symmetric)
            {
                symmetry = "symmetric";
            }
            else
            {
                symmetry = "general";
            }
            // Build header
            MMString.Append($"%%MatrixMarket matrix coordinate {type} {symmetry}\n");
            // Comments may be added here
            // Add Dimensions and non-zero count
            MMString.Append($"{RowCount} {ColumnCount} {NonZeros}\n");
            for (int i = 0; i < RowCount; i++)
            {
                for (int j = 0; j < ColumnCount; j++)
                    if (this[i, j] != 0)
                    {
                        MMString.Append($"{i} {j} {this[i, j]}\n");
                    }
            }
            return MMString.ToString();
        }
    }
}