namespace SparseTransform.DataStructures
{
    /// <summary>
    /// Represents symmetric matrices as a graph.
    /// </summary>
    public class AdjacencyGraph : IGraph
    {
        /// <summary>
        /// Stores the nodes.
        /// </summary>
        /// <remarks>
        /// In the matrix representation this is the lower left triangle since <see cref="AdjacencyGraph"/> represents symmetric matrices.
        /// </remarks>
        private Dictionary<int, GraphNode> _nodes;

        /// <summary>
        /// Number of nodes
        /// </summary>
        public int NodeCount
        {
            get
            {
                return _nodes.Count;
            }
        }

        private int _maxDegree = 0;
        /// <summary>
        /// Get the degree of the node with most neighbors.
        /// </summary>
        public int MaxDegree
        { get => _maxDegree; }

        private bool _colored = false;
        /// <summary>
        /// True if nodes are colored. To be set by Colorer. Cannot be unset.
        /// </summary>
        public bool Colored
        {
            get => _colored;
            set
            {
                if (_colored == false)
                    _colored = true;
            }
        }

        private int _colorsUsed = 0;
        /// <summary>
        /// Number of colors used for coloring.
        /// </summary>
        public int ColorsUsed
        {
            get
            {
                if (this._colorsUsed == 0)
                {
                    List<int> colors = new List<int>();
                    if (Colored)
                    {
                        foreach (GraphNode node in _nodes.Values)
                        {
                            int color = node.Color;
                            // Do not check for != 0 since we already know everything here is colored
                            if (!colors.Contains(color))
                            {
                                colors.Add(color);
                            }
                        }
                    }
                    this._colorsUsed = colors.Count;
                }
                return this._colorsUsed;
            }
        }

        /// <summary>
        /// Initialize adjacency graph with no nodes.
        /// </summary>
        public AdjacencyGraph()
        {
            _nodes = new Dictionary<int, GraphNode>();
        }

        public void AddEdge(int i, int j)
        {
            if (i != j)
            {
                GraphNode iNode = AddNode(i);
                GraphNode jNode = AddNode(j);
                iNode.AddEdge(jNode);
                jNode.AddEdge(iNode);
                // Update degree
                if (iNode.Degree > MaxDegree)
                    _maxDegree = iNode.Degree;
                if (jNode.Degree > MaxDegree)
                    _maxDegree = jNode.Degree;
            }
        }

        /// <summary>
        /// Adds a node to the graph and returns a reference to the added node.
        /// If the node exists, simply return a reference to it.
        /// </summary>
        /// <param name="index">index to create node for</param>
        /// <returns>GraphNode that was just added</returns>
        private GraphNode AddNode(int index)
        {
            if(!_nodes.ContainsKey(index))
            {
                GraphNode n = new GraphNode(index);
                _nodes[index] = n;
                return n;
            }
            else
            {
                return _nodes[index];
            }
        }

        /// <summary>
        /// Returns nodes sorted by index in ascending order as IEnumerable
        /// </summary>
        /// <returns>IEnumerable for the nodes</returns>
        public IEnumerable<GraphNode> GetNodes()
        {
            List<GraphNode> nodes = new List<GraphNode>(_nodes.Values);
            nodes.Sort((x, y) =>
            {
                if (x.Index < y.Index)
                    return -1;
                else
                    return 1;
            });
            return nodes as IEnumerable<GraphNode>;
        }

        public IntegerMatrix? ToSeedMatrix(int rows)
        {
            IntegerMatrix seed = new IntegerMatrix(rows, ColorsUsed);
            // This only works for row OR column partitions not mixed
            if (Colored)
            {
                foreach (GraphNode node in _nodes.Values)
                {
                    // The row index is the column index of the element
                    // The column index = binary vector index is the color
                    // Result: Every column of the seed matrix corresponds to one color
                    seed[node.Index - 1, node.Color - 1] = 1;
                }
                return seed;
            }
            else
            {
                // If graph is uncolored a null matrix is returned
                return null;
            }
        }

    }
}