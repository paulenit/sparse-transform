namespace SparseTransform.DataStructures
{
    /// <summary>
    /// Generic base type of matrices.
    /// Does not provide some algebra operations.
    /// Use IntegerMatrix and DoubleMatrix variants for that.
    /// </summary>
    /// <typeparam name="T">type of argument to store in the matrix</typeparam>
    public class Matrix<T>
    {
        /// <summary>
        /// Data structure is simple 2D array
        /// </summary>
        protected T[,] _matrixData;

        /// <summary>
        /// Indexer to make matrix accessible like a 2D array
        /// </summary>
        /// <param name="row">row index</param>
        /// <param name="column">column index</param>
        /// <returns></returns>
        public T this[int row, int column]
        {
            get { return _matrixData[row, column]; }

            set { _matrixData[row, column] = value; }
        }

        /// <summary>
        /// Number of rows
        /// </summary>
        public int RowCount
        { get; }

        /// <summary>
        /// Number of Columns
        /// </summary>
        public int ColumnCount
        { get; }

        /// <summary>
        /// true, if this matrix is symmetric.
        /// Equality is tested using the Object.Equal function.
        /// </summary>
        public bool Symmetric
        {
            get
            {
                if(RowCount != ColumnCount)
                {
                    return false;
                }
                for (int i = 0; i < RowCount; i++)
                {
                    for (int j = 0; j < ColumnCount; j++)
                    {
                        // Fail if side is null but the other is not (basically XOR)
                        if (this[i, j] is null || this[j, i] == null)
                        {
                            if (!(this[i, j] == null && this[j, i] == null))
                            {
                                return false;
                            }
                        }
                        else if (!this[i, j].Equals(this[j, i]))
                        {
                            return false;
                        }
                    }
                }
                return true;
            }
        }

        /// <summary>
        /// Initialize a matrix with the default value of T
        /// </summary>
        /// <param name="rows">number of rows in matrix</param>
        /// <param name="columns">number of columns in matrix</param>
        public Matrix(int rows, int columns)
        {
            _matrixData = new T[rows, columns];
            RowCount = rows;
            ColumnCount = columns;
        }
    }
}