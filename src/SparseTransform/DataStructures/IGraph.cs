namespace SparseTransform.DataStructures
{
    /// <summary>
    /// Base interface to be implemented by all graphs.
    /// Provides members for common functionality which is limited due to implementation differences in Graph implementations.
    /// </summary>
    public interface IGraph
    {
        public int ColorsUsed { get; }

        /// <summary>
        /// Adds an edge between nodes i and j. If either of the nodes is missing it is created.
        /// </summary>
        /// <param name="i">first node</param>
        /// <param name="j">second node</param>
        public void AddEdge(int i, int j);

        /// <summary>
        /// Transforms the graph to seed matrix representing the partitioning information.
        /// Requires information from the input matrix.
        /// Column count is inferred from the coloring information
        /// </summary>
        /// <param name="m">number of rows. Equal to the rows of the input matrix</param>
        /// <returns>matrix representing the partitioning</returns>
        public IntegerMatrix? ToSeedMatrix(int m);
    }
}